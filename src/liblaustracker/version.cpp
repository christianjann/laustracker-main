/*
 * This file is part of Laustracker.
 *
 * Copyright (C) 2013 Christian Jann <christian.jann@ymail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <laustracker/version.h>
#include "libraryinfo.h"

EXTERNC int projectname_version_major()
{
    return APPLICATION_VERSION_MAJOR;
}

EXTERNC int projectname_version_minor()
{
    return APPLICATION_VERSION_MINOR;
}

EXTERNC int projectname_version_patch()
{
    return APPLICATION_VERSION_PATCH;
}
